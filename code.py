"""Toy program for Adafruit Metro M4 AirLift microcontroller."""
import time

import board
from digitalio import DigitalInOut, Direction
from neopixel import NeoPixel
from busio import SPI
from adafruit_esp32spi import adafruit_esp32spi
from adafruit_ntp import NTP


def init_esp():
    """Initialize ESP co-processor for WiFi."""
    esp32_cs = DigitalInOut(board.ESP_CS)       # pylint: disable=no-member
    esp32_ready = DigitalInOut(board.ESP_BUSY)  # pylint: disable=no-member
    esp32_reset = DigitalInOut(board.ESP_RESET)  # pylint: disable=no-member
    spi = SPI(board.SCK, board.MOSI, board.MISO)
    return adafruit_esp32spi.ESP_SPIcontrol(spi, esp32_cs, esp32_ready,
                                            esp32_reset)


def init_wifi(esp):
    """Initialize WiFi using local credentials."""
    if esp.status == adafruit_esp32spi.WL_IDLE_STATUS:
        print("ESP32 found and in idle mode.  Firmware version: {}".format(
            ''.join([chr(c) for c in esp.firmware_version if c != 0])))
        print("Connecting to wifi ...")
    with open('ap.txt', 'br') as handle:
        # Read wifi credentials.
        ssid, passwd, _ = handle.read().split(b'\n')
    while not esp.is_connected:
        try:
            esp.connect_AP(ssid, passwd)
        except RuntimeError as err:
            print("Could not connect to wifi, retrying: ", err)
            continue
    del ssid, passwd


def init_ntp(esp):
    """Synchronize time using Network Time Protocol (NTP)."""
    ntp = NTP(esp)
    while not ntp.valid_time:
        print("Setting NTP time ...")
        ntp.set_time(-4)
        time.sleep(5)


def init_leds():
    """Return pins to access LoLshield Charlieplexed LEDs."""
    # Hard code the sheild's control pins D2-D13.
    pins = [DigitalInOut(getattr(board, pin))
            for pin in ['D{}'.format(i) for i in range(2, 14)]]
    # Change all pins into inputs to emulate open-circuit to turn off board.
    for pin in pins:
        pin.direction = Direction.INPUT
    return pins


def led_on(led):
    """Turn on a single LoLshield LED."""
    if led is None:
        return
    led[0].direction = Direction.OUTPUT
    led[0].value = True
    led[1].direction = Direction.OUTPUT
    led[1].value = False


def led_off(led):
    """Turn off a single LoLshield LED."""
    if led is None:
        return
    led[0].direction = Direction.INPUT
    led[1].direction = Direction.INPUT


PINS = init_leds()


def L(high, low):               # pylint: disable=invalid-name
    """Return LED high-low pair from named digital pins."""
    return (PINS[high - 2], PINS[low - 2],)


# Row major ordering of LEDs from
# https://github.com/jprodgers/LoLshield/blob/master/Charliplexing.cpp
#
# pylint: disable=bad-whitespace
# flake8: noqa
LEDS = [
    L(13,  5), L(13, 6), L(13,  7), L(13, 8), L(13,  9), L(13, 10), L(13, 11),
    L(13, 12), L(13, 4), L( 4, 13), L(13, 3), L( 3, 13), L(13,  2), L( 2, 13),
    L(12,  5), L(12, 6), L(12,  7), L(12, 8), L(12,  9), L(12, 10), L(12, 11),
    L(12, 13), L(12, 4), L( 4, 12), L(12, 3), L( 3, 12), L(12,  2), L( 2, 12),
    L(11,  5), L(11, 6), L(11,  7), L(11, 8), L(11,  9), L(11, 10), L(11, 12),
    L(11, 13), L(11, 4), L( 4, 11), L(11, 3), L( 3, 11), L(11,  2), L( 2, 11),
    L(10,  5), L(10, 6), L(10,  7), L(10, 8), L(10,  9), L(10, 11), L(10, 12),
    L(10, 13), L(10, 4), L( 4, 10), L(10, 3), L( 3, 10), L(10,  2), L( 2, 10),
    L( 9,  5), L( 9, 6), L( 9,  7), L( 9, 8), L( 9, 10), L( 9, 11), L( 9, 12),
    L( 9, 13), L( 9, 4), L( 4,  9), L( 9, 3), L( 3,  9), L( 9,  2), L( 2,  9),
    L( 8,  5), L( 8, 6), L( 8,  7), L( 8, 9), L( 8, 10), L( 8, 11), L( 8, 12),
    L( 8, 13), L( 8, 4), L( 4,  8), L( 8, 3), L( 3,  8), L( 8,  2), L( 2,  8),
    L( 7,  5), L( 7, 6), L( 7,  8), L( 7, 9), L( 7, 10), L( 7, 11), L( 7, 12),
    L( 7, 13), L( 7, 4), L( 4,  7), L( 7, 3), L( 3,  7), L( 7,  2), L( 2,  7),
    L( 6,  5), L( 6, 7), L( 6,  8), L( 6, 9), L( 6, 10), L( 6, 11), L( 6, 12),
    L( 6, 13), L( 6, 4), L( 4,  6), L( 6, 3), L( 3,  6), L( 6,  2), L( 2,  6),
    L( 5,  6), L( 5, 7), L( 5,  8), L( 5, 9), L( 5, 10), L( 5, 11), L( 5, 12),
    L( 5, 13), L( 5, 4), L( 4,  5), L( 5, 3), L( 3,  5), L( 5,  2), L( 2,  5),
]
# Clock digits.
#
# Seven segment display
# https://en.wikipedia.org/wiki/Seven-segment_display#Hexadecimal
A = [29, 30, 31]
B = [31, 45, 59]
C = [59, 73, 87]
D = [85, 86, 87]
E = [57, 71, 85]
F = [29, 43, 57]
G = [57, 58, 59]
DIGITS = [
    set(A + B + C + D + E + F),
    set(B + C),
    set(A + B + G + E + D),
    set(A + B + G + C + D),
    set(F + G + B + C),
    set(A + F + G + C + D),
    set(A + F + G + C + D + E),
    set(A + B + C),
    set(A + B + C + D + E + F + G),
    set(A + B + C + F + G),
]
# Hours and minutes digit offsets.
CLOCK_DIGIT_OFFSETS = [-2, 2, 6, 10]


def display_clock(hours, minutes):
    """Print clock time."""
    assert 0 <= hours <= 12
    assert 0 <= minutes <= 59
    # Convert time into individual digits.
    digits = [
        hours // 10,
        hours % 10,
        minutes // 10,
        minutes % 10,
    ]
    # Strip any leading 0 from hours position.
    offsets = CLOCK_DIGIT_OFFSETS
    if digits[0] == 0:
        digits = digits[1:]
        offsets = offsets[1:]
    # Gather LED set of digits into a frame for rendering.
    frame = {i + offset
             for digit, offset in zip(digits, offsets)
             for i in DIGITS[digit]}
    # Render.
    counter = 0
    while counter < 1e4 / len(frame):
        for i in frame:
            led_on(LEDS[i])
            led_off(LEDS[i])
        counter += 1


if __name__ == '__main__':
    # Initialize the only NeoPixel and change colors to indicate initialization
    # progress.
    dot = NeoPixel(board.NEOPIXEL,  # pylint: disable=no-member
                   1, brightness=0.01)
    color = {
        'purple': (127, 0, 127),
        'lime': (127, 127, 0),
        'black': (0, 0, 0),
    }
    # Connect to WiFi.
    dot[0] = color['purple']
    esp32 = init_esp()
    init_wifi(esp32)
    # Read network time.
    dot[0] = color['lime']
    init_ntp(esp32)
    # Initialization complete.
    dot[0] = color['black']

    # Main loop.
    while True:
        t = time.localtime()
        hr12 = t.tm_hour - 12 if t.tm_hour > 12 else t.tm_hour
        display_clock(hr12, t.tm_min)
